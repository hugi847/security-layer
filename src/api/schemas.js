const Joi = require('joi');

module.exports = (() => {
  const userSchema = Joi.object().keys({
    channel: Joi.string()
      .valid(['WEBC', 'WADM', 'MGMT'])
      .required(),
    userID: Joi.string()
      .email({ minDomainAtoms: 2 })
      .required(),
    password: Joi.string()
      .min(8)
      .required()
  });

  return {
    userSchema
  };
})();
