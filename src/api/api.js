const apiPath = '/api',
  apiConnection = require('../util/apiConnection');

module.exports = app => {
  app.options(`${apiPath}/*`, (req, res) => {
    res
      .status(200)
      .send('POST,GET,PUT,DELETE')
      .end();
  });

  /**
   * Valida las credenciales de acceso de un usuario y si es válido, asigna un token de acceso para el consumo de los
   * servicios disponibles en el backend.
   */
  app.all(`${apiPath}/*`, (req, res) => {
    let uri = req.url.replace(/[0-9a-f]{24}/, ':id'),
      access;

    apiConnection
      .send(`/api/access/${req.headers['access-token']}`, 'GET', {})
      .catch(err => {
        return Promise.reject({
          statusCode: 401,
          statusText: 'Invalid Access Token'
        });
        return Promise.reject(false);
      })
      .then(_access => {
        access = _access;
        return apiConnection.send(
          `/api/controls/`,
          'GET',
          {},
          {
            channel: access.channel,
            user: access.user,
            methods: req.method.toUpperCase()
          }
        );
      })
      .catch(err => {
        return Promise.reject({
          statusCode: 403,
          statusText: 'Unauthorized to access uri ' + uri + ' with method ' + req.method
        });
      })
      .then(controls => {
        let authorized = false;
        found: for (control of controls) {
          for (controlUri of control.uris) {
            if (controlUri === uri || uri.match('^' + controlUri + '$')) {
              authorized = true;
              break found;
            }
          }
        }

        if (!authorized) {
          return Promise.reject({
            statusCode: 403,
            statusText: 'Unauthorized to access uri ' + uri + ' with method ' + req.method
          });
        }

        let headers = {
          customer: access.customer,
          user: access.user,
          channel: access.channel
        };

        return apiConnection.send(
          req.path,
          req.method.toUpperCase(),
          headers,
          (req.method === 'GET' || req.method === 'DELETE')
            ? req.query :
            req.body,
          res,
          (req.method !== 'GET' || req.method !== 'DELETE')
            ? req.query :
            null,
        );
      })
      .catch(err => {
        if (false !== err) {
          res
            .header('Content-Type', 'application/json')
            .status(err.statusCode)
            .send(err)
            .end();
        }
      });
  });
};
