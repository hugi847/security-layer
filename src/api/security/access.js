const apiPath = '/security',
  apiConnection = require('../../util/apiConnection'),
  schemas = require('../schemas'),
  crypto = require('crypto');

module.exports = app => {
  hash = value => {
    if (value) {
      return crypto
        .createHash('MD5')
        .update(value)
        .digest('hex');
    } else {
      return null;
    }
  };

  app.options(`${apiPath}/*`, (req, res) => {
    res
      .status(200)
      .send('POST,GET,DELETE')
      .end();
  });

  /**
   * Valida las credenciales de acceso de un usuario y si es válido, asigna un token de acceso para el consumo de los
   * servicios disponibles en el backend.
   */
  app.post(`${apiPath}/access`, (req, res) => {
    let user, customer, userData;

    apiConnection
      .validate(req.body, schemas.userSchema, res)
      .then(validUserData => {
        delete validUserData.password;
        return apiConnection.send(`/api/users/access`, 'GET', {}, validUserData);
      })
      .then(_user => {
        user = _user;
        if (user.status !== 'ACTIVE') {
          res
            .header('Content-Type', 'application/json')
            .status(401)
            .send({ statusCode: 401, statusText: 'User status ' + user.status })
            .end();
          return Promise.reject(false);
        }

        if (hash(req.body.password) !== user.password) {
          res
            .header('Content-Type', 'application/json')
            .status(401)
            .send({ statusCode: 401, statusText: 'User/Password invalid 2' })
            .end();
          return Promise.reject(false);
        }

        return apiConnection.send(`/api/customers/info`, 'GET', { customer: user.customer });
      })
      .then(_customer => {
        customer = _customer;
        const accessData = {
          user: user._id.$oid,
          customer: customer._id.$oid,
          channel: user.channel
        };

        return apiConnection.send(`/api/access`, 'POST', {}, accessData);
      })
      .then(access => {
        const userData = {
          customerID: customer.customerID,
          name: customer.name,
          lastName: customer.lastName,
          email: customer.email
        };

        res
          .header('Content-Type', 'application/json')
          .header('Access-Token', access._id.$oid)
          .status(200)
          .send(userData)
          .end();
      })
      .catch(err => {
        if (err !== false) {
          console.log('err', err);
          res
            .header('Content-Type', 'application/json')
            .status(401)
            .send({ statusCode: 401, statusText: 'User/Password invalid 3' })
            .end();
        }
      });
  });

  /**
   * Valida un token de acceso de un usuario y si es válido, recupera la información del acceso para el consumo de los
   * servicios disponibles en el backend.
   */
  app.get(`${apiPath}/access`, (req, res) => {
    let user, customer, access;

    apiConnection
      .send(`/api/access/${req.headers['access-token']}`, 'GET', {})
      .then(_access => {
        access = _access;
        return apiConnection.send(`/api/users/${access.user}`, 'GET');
      })
      .then(_user => {
        user = _user;
        if (user.status !== 'ACTIVE') {
          res
            .header('Content-Type', 'application/json')
            .status(401)
            .send({ statusCode: 401, statusText: 'User status ' + user.status })
            .end();
          return;
        }

        return apiConnection.send(`/api/customers/info`, 'GET', { customer: access.customer });
      })
      .then(_customer => {
        customer = _customer;
        const userData = {
          customerID: customer.customerID,
          name: customer.name,
          lastName: customer.lastName,
          email: customer.email
        };

        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(userData)
          .end();
      })
      .catch(err => {
        console.log('err', err);

        res
          .header('Content-Type', 'application/json')
          .status(401)
          .send({ statusCode: 401, statusText: 'User/Password invalid 3' })
          .end();
      });
  });

  /**
   * Elimina un token de acceso de un usuario.
   */
  app.delete(`${apiPath}/access`, (req, res) => {
    let user, customer, access;

    apiConnection
      .send(`/api/access/${req.headers['access-token']}`, 'DELETE', {})
      .then(_access => {
        access = _access;
        res
          .header('Content-Type', 'application/json')
          .status(202)
          .send({ statusCode: 202, statusText: 'Access deleted' })
          .end();
      })
      .catch(err => {
        console.log('err', err);

        res
          .header('Content-Type', 'application/json')
          .status(400)
          .send({ statusCode: 400, statusText: 'Access token invalid' })
          .end();
      });
  });
};
