const http = require('http'),
  querystring = require('querystring'),
  Joi = require('joi'),
  path = require('path'),
  winston = require('winston'),
  logger = winston.createLogger({
    level: 'info',
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.label(''),
      winston.format.timestamp(),
      winston.format.printf(
        log => `[${log.timestamp}],[${path.relative('src', __filename)}],[${log.level}]: ${log.message}`
      )
    )
  });

module.exports = (() => {
  const api = '10.0.2.15',
    cURI = ``;

  const getAPIhttpOpts = resource => {
    return {
      hostname: api,
      port: 3000,
      path: `${cURI}${resource || ''}`,
      agent: false,
      headers: {
        'Content-Type': 'application/json'
      }
    };
  };

  const validate = (data, joiSchema, res) => {
    let result = Joi.validate(data, joiSchema, (err, data) => {
      if (err) {
        logger.log('error', err);

        res
          .header('Content-Type', 'application/json')
          .status(400)
          .send({
            statusCode: 400,
            statusText: err.details
              .reduce((sal, act) => {
                sal.push(act.message);
                return sal;
              }, [])
              .join(',')
          })
          .end();
        return false;
      }
      return data;
    });

    return result ? Promise.resolve(result) : Promise.reject(result);
  };

  const send = (path, method, headers, data, httpResponse, queryParams) => {
    const APIhttpOpts = getAPIhttpOpts(path);
    return new Promise((resolve, reject) => {
      let reqBody = (method === 'POST' || method === 'PUT') && data ? JSON.stringify(data) : '';
      let params =
        method === 'GET' || method === 'DELETE'
          ? '?' + querystring.stringify(data || {})
          : queryParams
            ? '?' + querystring.stringify(queryParams)
            : '';
      logger.log('debug', `requestBody: ${reqBody}`);

      logger.log('info', querystring.unescape(`${method} ${APIhttpOpts.path}${params ? params : ''}`));

      http
        .request(
          Object.assign(APIhttpOpts, {
            path: `${APIhttpOpts.path}${params ? params : ''}`,
            method: method,
            headers: Object.assign(headers || {}, {
              'Content-Type': 'application/json',
              'Content-Length': Buffer.byteLength(reqBody)
            })
          }),
          response => {
            logger.log('info', `Response HTTP Code: ${response.statusCode}`);

            var bodyResponse = [];
            response.on('data', data => {
              bodyResponse.push(data);
            });

            response.on('end', () => {
              try {
                const fullBodyResponse = Buffer.concat(bodyResponse).toString();
                logger.log('debug', `responseBody: ${fullBodyResponse}`);
                if (200 === response.statusCode) {
                  let responseObject = JSON.parse(fullBodyResponse);
                  if (
                    method === 'GET' &&
                    (!responseObject || (responseObject instanceof Array && !responseObject.length))
                  ) {
                    reject({
                      statusCode: 404,
                      statusText: 'Document not found'
                    });
                  } else {
                    resolve(responseObject);
                  }
                } else {
                  console.log('fullBodyResponse', fullBodyResponse);

                  return reject(JSON.parse(fullBodyResponse));
                }
              } catch (err) {
                logger.log('error', err);
                reject({
                  statusCode: 500,
                  statusText: err.message || 'Internal server error'
                });
              }
            });
          }
        )
        .on('error', err => {
          reject({ statusCode: 500, statusText: 'Internal server error' });
        })
        .end(reqBody);
    })
      .then(response => {
        if (!httpResponse) {
          return Promise.resolve(response);
        }
        httpResponse
          .header('Content-Type', 'application/json')
          .send(JSON.stringify(response))
          .end();
      })
      .catch(err => {
        if (!httpResponse) {
          logger.log('error', 'SEND error: ' + (typeof err === 'object' ? JSON.stringify(err) : err));
          return Promise.reject(err);
        }
        httpResponse
          .status(err.statusCode || 500)
          .header('Content-Type', 'application/json')
          .send(JSON.stringify({ statusCode: err.statusCode || 500, statusText: err.statusText || err.message || err }))
          .end();
      });
  };

  return {
    cURI,
    getAPIhttpOpts,
    validate,
    send
  };
})();
